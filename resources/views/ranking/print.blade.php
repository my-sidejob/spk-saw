<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Hasil</title>
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center;
        }
        .header p, .header h2 {
            margin-bottom: 0;
            margin-top: 0;
            line-height: 1.5em;
        }
        .table {
            font-size: 13px;
            width: 100%;
            text-align: left;
            border-spacing: 0;
            border-top:1px solid black;
            border-right: 1px solid black;
        }
        .table th, .table td {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
            padding: 6px 10px;
            text-align: left;
        }
        .d-none {
            display: none!important;
        }
        h2.title {
            text-align: center;
            font-size:20px;
        }
    </style>
</head>
<body>

    <div class="text-center header">
        <div style="margin-bottom:10px">
            <img src="{{ public_path('img/logo.jpg') }}" alt="logo" width="80">
        </div>
        <h2>{{ env("APP_NAME") }}</h2>
        <p>{{ env("APP_ADDRESS") }}</p>
        <p>E: {{ env("APP_EMAIL") }}  &nbsp; P: {{ env("APP_PHONE") }}</p>
    </div>

    <hr>
    @if(request()->get('dari') == null)
    <h2 class="title">Laporan Kredit</h2>
    @else
    <p>Laporan Kredit dari tanggal {{ request()->get('dari') }} sampai tanggal {{ request()->get('sampai') }} dengan {{ request()->get('status') == 0 ? 'semua status kredit' : 'status ' . getStatusKredit(request()->get('status')) }}</p>
    @endif
    <div class="table-responsive">
        <table class="table align-items-center table-flush data-table">
            <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>Pemohon / Alternatif</th>
                    <th>Alamat</th>
                    <th>No. Telp</th>
                    <th>Tgl. Pengajuan</th>
                    <th>Hasil</th>
                    <th>Persentase</th>
                    <th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
                <?php $iteration = 1; ?>
                @foreach($ranking as $row)
                    <?php
                        if(!empty(request()->get('dari'))) {
                            $dari = request()->get('dari');
                            $sampai = request()->get('sampai');
                            $tgl = ($row->pemohon->pendukung->tgl_pengajuan ?? '');
                            if($tgl != ''){
                                if($tgl >= $dari && $tgl <= $sampai) {
                                    if(request()->get('status') == 1 ){
                                        if($row['persentase'] > 50) {
                                            $show = true;
                                        } else {
                                            $show = false;
                                        }
                                    } else if (request()->get('status') == 2) {
                                        if($row['persentase'] < 50) {
                                            $show = true;
                                        } else {
                                            $show = false;
                                        }
                                    } else {
                                        $show = true;
                                    }
                                } else {
                                    $show = false;
                                }
                            } else {
                                $show = false;
                            }
                        } else {
                            $show = true;
                        }
                    ?>
                    <tr class="{{ $show ? '' : 'd-none' }}">
                        <td>{{ $show == true ? $iteration++ : 0 }}</td>
                        <td>{{ 'A'.$row->pemohon->id . ' - ' . $row->pemohon->nama }}</td>
                        <td>{{ $row->pemohon->alamat }}</td>
                        <td>{{ $row->pemohon->no_telp }}</td>
                        <td>{{ $row->pemohon->pendukung->tgl_pengajuan ?? 'N/A' }}</td>
                        <td>
                            {{ $row['total'] }}
                        </td>
                        <td>{{ $row['persentase'] }}%</td>
                        <td>{!! checkKelayakan($row['persentase']) !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>
</html>
