@extends('layouts.app')

@section('title', 'Ranking')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Hasil</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Hasil dan Rangking</h3>
                            </div>
                            {{-- <div class="col text-right">
                                <a href="{{ route('ranking.print') }}" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Print</a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush data-table">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon / Alternatif</th>
                                    <th>Hasil Perhitungan</th>
                                    <th>Persentase</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ranking as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ 'A'.$row->pemohon->id . ' - ' . $row->pemohon->nama }}</td>
                                        <td>
                                            {{ $row['total'] }}
                                        </td>
                                        <td>{{ $row['persentase'] }}%</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
        $('.data-table').DataTable({
            "pageLength": 100,
            "paging":   false,
            "info":   false,
            "searching":   false,
        });
    } );
</script>

@endpush