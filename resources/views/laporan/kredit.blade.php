@extends('layouts.app')

@section('title', 'Laporan Kredit')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Hasil</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Filter Berdasarkan Tanggal Pengajuan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="">Dari</label>
                                    <input type="text" class="form-control datepicker" name="dari" value="{{ request()->get('dari') }}">
                                </div>
                                <div class="col-md-5">
                                    <label for="">Sampai</label>
                                    <input type="text" class="form-control datepicker" name="sampai" value="{{ request()->get('sampai') }}">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Kelayakan</label>
                                    <select name="status" class="form-control">
                                        <option value="0">Semua</option>
                                        <option value="1" {{ request()->get('status') == 1 ? 'selected' : '' }}>Layak</option>
                                        <option value="2" {{ request()->get('status') == 2 ? 'selected' : '' }}>Tidak Layak</option>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right mt-3">
                                    <input type="submit" value="Cari" class="btn btn-info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Laporan Kredit</h3>
                            </div>
                            <div class="col text-right">
                                @if(request()->get('dari') == null)
                                    <a href="{{ route('ranking.print') }}" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Print</a>
                                @else
                                    <a href="{{ route('ranking.print') }}?dari={{ request()->get('dari') }}&sampai={{ request()->get('sampai') }}&status={{ request()->get('status') }}" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Print</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush data-table">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon / Alternatif</th>
                                    <th>Platform Pinjaman</th>
                                    <th>Tgl Pengajuan</th>
                                    <th>Hasil Perhitungan</th>
                                    <th>Persentase</th>
                                    <th>Kelayakan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $iteration = 1; ?>
                                @foreach($ranking as $row)
                                    <?php
                                        if(!empty(request()->get('dari'))) {
                                            $dari = request()->get('dari');
                                            $sampai = request()->get('sampai');
                                            $tgl = ($row->pemohon->pendukung->tgl_pengajuan ?? '');
                                            if($tgl != ''){
                                                if($tgl >= $dari && $tgl <= $sampai) {
                                                    if(request()->get('status') == 1 ){
                                                        if($row['persentase'] > 50) {
                                                            $show = true;
                                                        } else {
                                                            $show = false;
                                                        }
                                                    } else if (request()->get('status') == 2) {
                                                        if($row['persentase'] < 50) {
                                                            $show = true;
                                                        } else {
                                                            $show = false;
                                                        }
                                                    } else {
                                                        $show = true;
                                                    }
                                                } else {
                                                    $show = false;
                                                }
                                            } else {
                                                $show = false;
                                            }
                                        } else {
                                            $show = true;
                                        }
                                    ?>
                                    <tr class="{{ $show ? '' : 'd-none' }}">
                                        <td>{{ $show == true ? $iteration++ : 0 }}</td>
                                        <td>{{ 'A'.$row->pemohon->id . ' - ' . $row->pemohon->nama }}</td>
                                        <td>{{ 'Rp. ' . number_format($row->pemohon->platform_pinjaman) }}</td>
                                        <td>{{ $row->pemohon->pendukung->tgl_pengajuan ?? 'N/A' }}</td>
                                        <td>
                                            {{ addZeroIfNeeded($row['total']) }}
                                        </td>
                                        <td>{{ $row['persentase'] }}%</td>
                                        <td>{!! checkKelayakan($row['persentase']) !!}</td>
                                        <td><a href="{{ route('pendukung.show', $row->pemohon->id) }}" class="btn btn-sm btn-success text-white">Detail</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
        $('.data-table').DataTable({
            "pageLength": 100,
            "paging":   false,
            "info":   false,
            "searching":   false,
        });
    } );
</script>

@endpush