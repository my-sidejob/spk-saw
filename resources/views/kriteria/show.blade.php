@extends('layouts.app')

@section('title', 'Kriteria')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Detail Kriteria</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ $kriteria->kode . ' - ' . $kriteria->nama }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Keterangan</th>
                                    <th>Skor</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($table_detail_kriteria ?? [] as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->keterangan }}</td>
                                        <td>{{ $row->skor }}</td>
                                        <td>
                                            <form action="{{ route('detail-kriteria.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                {{-- <a href="{{ route('detail-kriteria.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a> --}}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                      <div class="row align-items-center">
                        <div class="col-12">
                          <h3 class="mb-0">Form {{ $kriteria->kode . ' - ' . $kriteria->nama }}</h3>
                        </div>
                      </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ (!isset($detail_kriteria['id'])) ? route('detail-kriteria.store') : route('detail-kriteria.update', $detail_kriteria['id']) }}" method="post">
                            @csrf
                            @isset($detail_kriteria['id'])
                                {{ method_field('PUT')}}
                            @endisset
                            <input type="hidden" name="kriteria_id" value="{{ $kriteria->id }}">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea class="form-control" name="keterangan" rows="3">{{ old('keterangan', $detail_kriteria['keterangan']) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Skor</label>
                                        <input type="text" class="form-control" name="skor" {{ old('skor', $detail_kriteria['skor']) }}>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
