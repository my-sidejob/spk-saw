@extends('layouts.app')

@section('title', 'Form Kriteria')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Kriteria</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form kriteria</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($kriteria['id'])) ? route('kriteria.store') : route('kriteria.update', $kriteria['id']) }}" method="post">
                        @csrf
                        @isset($kriteria['id'])
                            {{ method_field('PUT')}}
                        @endisset
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Kode</label>
                                        <input type="text" class="form-control" name="kode" placeholder="Contoh: C1" value="{{ old('kode', $kriteria['kode']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama kriteria</label>
                                        <input type="text" class="form-control" name="nama" placeholder="Contoh: Character" value="{{ old('nama', $kriteria['nama']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Jenis Kriteria</label>
                                        <select name="jenis_kriteria" class="form-control">
                                            <option value="benefit" {{ old('jenis_kriteria', $kriteria['jenis_kriteria']) == "benefit" ? 'selected' : '' }}>Benefit</option>
                                            <option value="cost" {{ old('jenis_kriteria', $kriteria['jenis_kriteria']) == "cost" ? 'selected' : '' }}>Cost</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Bobot</label>
                                        <div class="input-group mb-3">
                                            <input type="number" name="bobot" value="{{ old('bobot', $kriteria['bobot']) }}" class="form-control" placeholder="Bobot" aria-label="Bobot" aria-describedby="basic">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea name="keterangan" value="{{ old('keterangan', $kriteria['keterangan']) }}" class="form-control" placeholder="keterangan"></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <input type="submit" value="Simpan" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>

@endsection
