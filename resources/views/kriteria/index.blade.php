@extends('layouts.app')

@section('title', 'Kriteria')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Kriteria</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Kriteria</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('kriteria.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Bobot</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kriteria as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->kode }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>{!! $row->getJenisKriteria() !!}</td>
                                        <td>{{ $row->bobot }}%</td>
                                        <td>{{ $row->keterangan }}</td>
                                        <td>
                                            <form action="{{ route('kriteria.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('kriteria.show', $row->id ) }}" class="btn btn-sm btn-success text-white" title="Detail Kriteria"><i class="fa fa-table"></i></a>
                                                <a href="{{ route('kriteria.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
