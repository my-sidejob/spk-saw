@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="header bg-gray-dark pb-5">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Dashboard</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mt-4">
        <div class="col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total Pemohon</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $total_pemohon }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="fa fa-users"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total Kriteria</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $total_kriteria }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-blue text-white rounded-circle shadow">
                    <i class="fa fa-clipboard-list"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total Aspek Penilaian</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $total_aspek_penilaian }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="fa fa-table"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @if(auth()->user()->level == 3 || auth()->user()->level == 4)
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col">
                    Chart
                  </div>
                  <div class="col text-right">
                    <select name="" id="changeYear">
                      <option value="2018" {{ request()->get('year') == '2018' ? 'selected' : '' }}>2018</option>
                      <option value="2019" {{ request()->get('year') == '2019' ? 'selected' : '' }}>2019</option>
                      <option value="2020" {{ request()->get('year') == '2020' ? 'selected' : '' }}>2020</option>
                      <option value="2021" {{ request()->get('year') == '2021' ? 'selected' : '' }}>2021</option>
                      <option value="2022" {{ request()->get('year') == '2022' || request()->get('year') == null ? 'selected' : '' }}>2022</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <p>Total Kredit</p>
                    <div>
                      <canvas id="totalKreditChart" height="260"></canvas>
                    </div>
                  </div>
                  <div class="col-12 mt-5">
                    <p>Total Layak</p>
                    <div>
                      <canvas id="chartLayak" height="260"></canvas>
                    </div>
                  </div>
                  <div class="col-12 mt-5">
                    <p>Total Tidak Layak</p>
                    <div>
                      <canvas id="chartTidakLayak" height="260"></canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endif

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p>Selamat datang di sistem pendukung keputusan menggunakan metode simple additive weight pada {{ env('APP_NAME') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection


@push('scripts')

<script>
  $('#changeYear').change(function(){
    window.location.href = "{{ url('/dashboard') }}?year="+$(this).val();
  })

</script>

<script>
  const labelTotalKredit = "{{ $monthTotalKredit }}".split(",");
  const dataTotalKredit = "{{ $dataTotalKredit }}".split(",");
  const totalKreditChart = new Chart(
    document.getElementById('totalKreditChart'),
    {
      type: 'line',
      data: {
        labels: labelTotalKredit,
        datasets: [
          {
            label: 'Total Kredit',
            data: dataTotalKredit,
            borderColor: '#3e95cd',
            fill: false,
            pointRadius: 4,
          }
        ]
      },
      options: {
        responsive: true,
      },
    }
  );

  const labelLayak = "{{ $monthLayak }}".split(",");
  const dataLayak = "{{ $dataLayak }}".split(",");
  const chartLayak = new Chart(
    document.getElementById('chartLayak'),
    {
      type: 'line',
      data: {
        labels: labelLayak,
        datasets: [
          {
            label: 'Total Layak',
            data: dataLayak,
            borderColor: '#3cba9f',
            fill: false,
            pointRadius: 4,
            pointBackgroundColor: '#3cba9f'
          }
        ]
      },
      options: {
        responsive: true,
      },
    }
  );

  const labelTidakLayak = "{{ $monthTidakLayak }}".split(",");
  const dataTidakLayak = "{{ $dataTidakLayak }}".split(",");
  const chartTidakLayak = new Chart(
    document.getElementById('chartTidakLayak'),
    {
      type: 'line',
      data: {
        labels: labelTidakLayak,
        datasets: [
          {
            label: 'Total TidakLayak',
            data: dataTidakLayak,
            borderColor: 'red',
            fill: false,
            pointRadius: 4,
            pointBackgroundColor: 'red'
          }
        ]
      },
      options: {
        responsive: true,
      },
    }
  );


</script>


@endpush