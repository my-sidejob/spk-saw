@extends('layouts.app')

@section('title', 'Data Pendukung')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Detail Pemohon</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Detail dan Pendukung</h3>
                            </div>
                            @if(auth()->user()->level == 1 || auth()->user()->level == 3)
                            <div class="col text-right">
                                <a href="{{ route('pemohon.edit', $pemohon->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-cog"></i> Update</a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>{{ $pemohon->nama }}</td>
                            </tr>
                            <tr>
                                <td>Kode</td>
                                <td>:</td>
                                <td>A{{ $pemohon->id }}</td>
                            </tr>
                            <tr>
                                <td>Tempat, Tanggal Lahir</td>
                                <td>:</td>
                                <td>{{ $pemohon->tmpt_lhr . ', ' . $pemohon->tgl_lhr }}</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>{{ $pemohon->alamat }}</td>
                            </tr>
                            <tr>
                                <td>J. Kel</td>
                                <td>:</td>
                                <td>{{ strtoupper($pemohon->j_kel) }}</td>
                            </tr>
                            <tr>
                                <td>Status Nikah</td>
                                <td>:</td>
                                <td>{{ $pemohon->getStatusNikah() }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td>{{ $pemohon->email }}</td>
                            </tr>
                            <tr>
                                <td>No. Telp</td>
                                <td>:</td>
                                <td>+62{{ $pemohon->no_telp }}</td>
                            </tr>
                            <tr>
                                <td>Platform Pinjaman</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($pemohon->platform_pinjaman) }}</td>
                            </tr>
                            <tr>
                                <td>Status BI Checking</td>
                                <td>:</td>
                                <td>{{ isset($pendukung->status_bi_checking) ? $pendukung->getStatusBIChecking() : '-' }}</td>
                            </tr>
                            <tr>
                                <td>Jangka Waktu</td>
                                <td>:</td>
                                <td>{{ $pendukung->jangka_waktu ?? '-' }} Bulan</td>
                            </tr>
                            <tr>
                                <td>Jenis Bunga</td>
                                <td>:</td>
                                <td>{!! isset($pendukung->jenis_bunga) ? $pendukung->getJenisBunga() : '-' !!}</td>
                            </tr>
                            <tr>
                                <td>Bunga</td>
                                <td>:</td>
                                <td>{{ $pendukung->bunga ?? '-' }}%</td>
                            </tr>
                            <tr>
                                <td>Angsuran</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($pendukung->angsuran ?? 0) }} Perbulan</td>
                            </tr>
                            <tr>
                                <td>Penghasilan</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($pendukung->penghasilan ?? 0) }} Perbulan</td>
                            </tr>
                            <tr>
                                <td>Jumlah Keluarga</td>
                                <td>:</td>
                                <td>{{ $pendukung->jumlah_keluarga ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Jaminan</td>
                                <td>:</td>
                                <td>{{ $pendukung->jaminan ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td>Kepemilikan Rumah</td>
                                <td>:</td>
                                <td>{!! isset($pendukung->kepemilikan_rumah) ? $pendukung->getKepemilikanRumah() : '-' !!}</td>
                            </tr>
                            <tr>
                                <td>Ditambahkan Oleh</td>
                                <td>:</td>
                                <td>{{ $pemohon->user->nama }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection