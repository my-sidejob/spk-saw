@extends('layouts.app')

@section('title', 'Form user')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Users</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form User</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ (!isset($user['id'])) ? route('user.store') : route('user.update', $user['id']) }}" method="post">
                        @csrf
                        @isset($user['id'])
                            {{ method_field('PUT')}}
                        @endisset
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama user</label>
                                        <input type="text" class="form-control" name="nama" placeholder="" value="{{ old('nama', $user['nama']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="" value="{{ old('email', $user['email']) }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Level</label>
                                        @if(auth()->user()->level === 3)
                                        <select name="level" class="form-control">
                                            <option value="1" {{ old('level', $user['level']) == "1" ? 'selected' : '' }}>Customer Service</option>
                                            <option value="2" {{ old('level', $user['level']) == "2" ? 'selected' : '' }}>Account Officer</option>
                                            <option value="3" {{ old('level', $user['level']) == "3" ? 'selected' : '' }}>Admin</option>
                                            <option value="4" {{ old('level', $user['level']) == "4" ? 'selected' : '' }}>Direksi</option>
                                        </select>
                                        @else
                                            <input type="text" name="" class="form-control" value="{{ auth()->user()->getLevel() }}" readonly>
                                            <input type="hidden" name="level" value="{{ auth()->user()->level }}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control" name="alamat" placeholder="">{{ old('alamat', $user['alamat']) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>No. Telp</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic">+62</span>
                                            </div>
                                            <input type="number" name="no_telp" value="{{ old('no_telp', $user['no_telp']) }}" class="form-control" placeholder="" aria-label="no_telp" aria-describedby="basic">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <input type="submit" value="Simpan" class="btn btn-success">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>

@endsection
