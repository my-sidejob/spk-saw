<nav class="navbar navbar-top navbar-expand navbar-dark bg-gray-dark border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center  ml-md-auto ">
                <a class="nav-link pr-0 topbar" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <li class="nav-item dropdown">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('img/user-icon.png') }}">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu  dropdown-menu-right topbar-menu">
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </button>
                    </form>
                </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
