
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show my-toast" role="alert">
    <strong>Sukses!</strong> {{ session()->get('success') }}.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span class="fa fa-times"></span>
    </button>
</div>
@endif

@if(session()->has('error'))
<div class="alert alert-danger alert-dismissible fade show my-toast" role="alert">
    <strong>Error!</strong> {{ session()->get('error') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span class="fa fa-times"></span>
    </button>
</div>
@endif

@if(session()->has('warning'))
<div class="alert alert-warning alert-dismissible fade show my-toast" role="alert">
    <strong>Warning!</strong> {{ session()->get('warning') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span class="fa fa-times"></span>
    </button>
</div>
@endif



@if($errors->any())
<div class="pb-3 pt-3 bg-danger">
    <ul style="margin-bottom:0">
    @foreach($errors->all() as $error)
        <li class="text-white">{{ $error }}</li>
    @endforeach
    </ul>

</div>
@endif
