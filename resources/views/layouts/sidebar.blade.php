<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="{{ asset('img/logo.jpg') }}" alt="">
          {{ env('APP_NAME') }}
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('dashboard') == true ) ? 'active' : '' }}" href="{{ url('dashboard') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            @if(auth()->user()->level == 2 || auth()->user()->level == 3)
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('kriteria') == true ) ? 'active' : '' }}" href="{{ url('kriteria') }}">
                <i class="fa fa-clipboard-list text-orange"></i>
                <span class="nav-link-text">Kriteria</span>
              </a>
            </li>
            @endif
            @if(auth()->user()->level != 4)
            <li class="nav-item">
                <a class="nav-link {{ (urlHasPrefix('pemohon') == true ) ? 'active' : '' }}" href="{{ url('pemohon') }}">
                  <i class="fa fa-users text-info"></i>
                  <span class="nav-link-text">Pemohon</span>
                </a>
            </li>
            @endif
            @if(auth()->user()->level == 3)
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('user') == true ) ? 'active' : '' }}" href="{{ url('user') }}">
                <i class="fa fa-user-tie text-primary"></i>
                <span class="nav-link-text">User</span>
              </a>
            </li>
            @endif
          </ul>
          @if(auth()->user()->level == 2 || auth()->user()->level == 3)
          <hr class="my-3">
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Perhitungan</span>
          </h6>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('penilaian') == true ) ? 'active' : '' }}" href="{{ url('penilaian') }}">
                <i class="fa fa-list-ol text-success"></i>
                <span class="nav-link-text">Penilaian Alternatif</span>
              </a>
            </li>
            {{-- <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('normalisasi') == true ) ? 'active' : '' }}" href="{{ url('normalisasi') }}">
                <i class="fa fa-clipboard-check text-danger"></i>
                <span class="nav-link-text">Matrix & Normalisasi</span>
              </a>
            </li> --}}
            <li class="nav-item">
              <a class="nav-link {{ (urlHasPrefix('hasil') == true ) ? 'active' : '' }}" href="{{ url('hasil') }}">
                <i class="fa fa-trophy text-yellow"></i>
                <span class="nav-link-text">Hasil Perhitungan</span>
              </a>
            </li>
          </ul>
          @endif
          <hr class="my-3">
            <h6 class="navbar-heading p-0 text-muted">
              <span class="docs-normal">Laporan</span>
            </h6>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link {{ (urlHasPrefix('laporan-kredit') == true ) ? 'active' : '' }}" href="{{ url('laporan-kredit') }}">
                  <i class="fa fa-envelope-open-text text-dark-gray"></i>
                  <span class="nav-link-text">Laporan Kredit</span>
                </a>
              </li>
            </ul>

          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">

            <li class="nav-item">
              <div class="nav-link active active-pro">
                <span class="nav-link-text">&copy; 2021</span>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
</nav>
