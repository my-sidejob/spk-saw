<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>@yield('title') - {{ env('APP_NAME') }}</title>
  <!-- Favicon -->
  <link rel="icon" href="#" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <link rel="stylesheet" href="{{ asset('vendor/nucleo/css/nucleo.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" type="text/css">
  <link href="{{ asset('vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{ asset('css/argon.css?v=1.3') }}" type="text/css">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css">
</head>

    @auth
    <body class="">
        <!-- Sidenav -->
        @include('layouts.sidebar')
        <!-- Main content -->
        <div class="main-content" id="panel">
        <!-- Topnav -->
        @include('layouts.topbar')

        @include('layouts.alert')

        @yield('content')

        </div>
    @endauth

    @guest()
    <body class="bg-default">
        @yield('content')
    @endguest
    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('vendor/js-cookie/js.cookie.js')}}"></script>
    <script src="{{ asset('vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script src="{{ asset('vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
    <!-- Optional JS -->
    <script src="{{ asset('vendor/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{ asset('vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <!-- Argon JS -->
    <script src="{{ asset('vendor/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('js/argon.js?v=1.2.0')}}"></script>
    <script>
        $( function() {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
            });
            $('.topbar').click(function(){
                $('.topbar-menu').toggleClass("show");
            });
            $('.close').click(function(){
                $('.my-toast').fadeOut();
            });
            $('.select2').select2();
        } );
    </script>
    @stack('scripts')
</body>

</html>
