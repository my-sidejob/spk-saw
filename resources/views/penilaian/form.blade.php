@extends('layouts.app')

@section('title', 'Form Penilaian')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Form Penilaian</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  <div class="row align-items-center">
                    <div class="col-12">
                      <h3 class="mb-0">Form Penilaian Pemohon / Alternatif</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                    <form action="{{ ($type == 'create') ? route('penilaian.store') : route('penilaian.update', $pemohon['id']) }}" method="post">
                        @csrf
                        @if($type != 'create')
                            {{ method_field('PUT')}}
                        @endif
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nama pemohon</label>
                                        @if($type == 'create')
                                            <select name="pemohon_id" class="form-control select2" required>
                                                <option value="">- Pilih -</option>
                                                @foreach($pemohon as $option)
                                                    <option value="{{ $option->id }}">{{ $option->nama }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" class="form-control" value="{{ $pemohon->nama }}" readonly>
                                            <input type="hidden" name="pemohon_id" value="{{ $pemohon->id }}">
                                        @endif
                                    </div>
                                </div>
                                @foreach($kriteria as $form)
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{ $form->kode . ' - ' . $form->nama }}</label>
                                            <select name="detail_kriteria_ids[]" id="" class="form-control" required>
                                                @foreach($form->detail_kriteria()->orderBy('skor', 'asc')->get() as $option)
                                                    <option value="{{ $option->id }}" {{ ($penilaian[$form->kode][0]->detail_kriteria_id ?? '') === $option->id ? 'selected' : ''  }}>{{ $option->keterangan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-md-12 text-right">
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection