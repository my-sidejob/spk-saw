@extends('layouts.app')

@section('title', 'Penilaian')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Penilaian</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Pemohon / Alternatif + Penilaian</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('penilaian.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon / Alternatif</th>
                                    @foreach($kriteria as $th)
                                        <th>{{ $th->kode }}</th>
                                    @endforeach
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($penilaian as $key => $row)
                                <?php
                                    $pemohon = getDetailAlternatif($key);
                                ?>
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ 'A'.$pemohon->id . ' - ' . $pemohon->nama }}</td>
                                    @foreach($row as $value)
                                        <td>{{ $value->detailKriteria->keterangan }}</td>
                                    @endforeach
                                    <td>
                                        <form action="{{ route('penilaian.destroy', $key) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <a href="{{ route('penilaian.show', $key) }}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                            <a href="{{ route('penilaian.edit', $key) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                            <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-end">
            <div class="col-md-6">
                <form action="{{ route('hitung') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-success btn-block" onclick="return confirm('Apakah anda yakin untuk mulai melakukan perhitungan?')"><i class="fa fa-calculator"></i> Hitung</button>
                </form>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Matrix Keputusan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon / Alternatif</th>
                                    @foreach($kriteria as $th)
                                        <th>{{ $th->kode }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($perhitungan as $key => $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ 'A'.$row[0]->pemohon->id . ' - ' . $row[0]->pemohon->nama }}</td>
                                        @foreach($row as $value)
                                            <td>{{ $value['matrix'] }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Tabel Normalisasi</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Pemohon / Alternatif</th>
                                    @foreach($kriteria as $th)
                                        <th>{{ $th->kode }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($perhitungan as $key => $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ 'A'.$row[0]->pemohon->id . ' - ' . $row[0]->pemohon->nama }}</td>
                                        @foreach($row as $value)
                                            <td>{{ $value['normalisasi'] }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection