@extends('layouts.app')

@section('title', 'Penilaian')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Detail Penilaian</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Penilaian {{ $pemohon->nama }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>{{ $pemohon->nama }}</td>
                                </tr>
                                @foreach($penilaian as $key => $value)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>:</td>
                                        <td>{{ $value[0]->ket ?? '' }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td>Ditambahkan oleh</td>
                                    <td>:</td>
                                    <td>{{ getUser($value[0]->user_id)->nama ?? '' }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection