@extends('layouts.app')

@section('title', 'Form Pemohon')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Pemohon</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <form action="{{ (!isset($pemohon['id'])) ? route('pemohon.store') : route('pemohon.update', $pemohon['id']) }}" method="post">
            <div class="row">
                @csrf
                @isset($pemohon['id'])
                    {{ method_field('PUT')}}
                @endisset
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-12">
                            <h3 class="mb-0">Form Pemohon / Alternatif</h3>
                            </div>
                        </div>
                        </div>
                        <div class="card-body">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Nama pemohon</label>
                                            <input type="text" class="form-control" name="nama" placeholder="" value="{{ old('nama', $pemohon['nama']) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tempat lahir</label>
                                            <input type="text" class="form-control" name="tmpt_lhr" placeholder="" value="{{ old('tmpt_lhr', $pemohon['tmpt_lhr']) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tanggal lahir</label>
                                            <input type="text" class="form-control datepicker" name="tgl_lhr" placeholder="" value="{{ old('tgl_lhr', $pemohon['tgl_lhr']) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" name="alamat" placeholder="">{{ old('alamat', $pemohon['alamat']) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <select name="j_kel" class="form-control">
                                                <option value="l" {{ old('j_kel', $pemohon['j_kel']) == "l" ? 'selected' : '' }}>Laki - Laki</option>
                                                <option value="p" {{ old('j_kel', $pemohon['j_kel']) == "p" ? 'selected' : '' }}>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Status Nikah</label>
                                            <select name="status_nikah" class="form-control">
                                                <option value="0" {{ old('status_nikah', $pemohon['status_nikah']) == 0 ? 'selected' : '' }}>Belum Menikah</option>
                                                <option value="1" {{ old('status_nikah', $pemohon['status_nikah']) == 1 ? 'selected' : '' }}>Menikah</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" placeholder="" value="{{ old('email', $pemohon['email']) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>No. Telp</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic">+62</span>
                                                </div>
                                                <input type="number" name="no_telp" value="{{ old('no_telp', $pemohon['no_telp']) }}" class="form-control" placeholder="" aria-label="no_telp" aria-describedby="basic">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Platform Pinjaman</label>
                                            <input type="text" class="form-control" name="platform_pinjaman" placeholder="" value="{{ old('platform_pinjaman', $pemohon['platform_pinjaman']) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-12">
                                    <h3 class="mb-0">Data Pendukung</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Tanggal Pengajuan</label>
                                            <input type="text" name="tgl_pengajuan" value="{{ old('tgl_pengajuan', ($pendukung['tgl_pengajuan'] ?? date('Y-m-d'))) }}" class="form-control datepicker" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Status BI Checking</label>
                                            <select name="status_bi_checking" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="1" {{ old('status_bi_checking', ($pendukung['status_bi_checking'] ?? '')) == 1 ? 'selected' : '' }}>Lancar</option>
                                                <option value="2" {{ old('status_bi_checking', ($pendukung['status_bi_checking'] ?? '')) == 2 ? 'selected' : '' }}>Diragukan</option>
                                                <option value="3" {{ old('status_bi_checking', ($pendukung['status_bi_checking'] ?? '')) == 3 ? 'selected' : '' }}>DPK</option>
                                                <option value="4" {{ old('status_bi_checking', ($pendukung['status_bi_checking'] ?? '')) == 4 ? 'selected' : '' }}>Tidak Lancar</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Jangka Waktu</label>
                                            <div class="input-group mb-3">
                                                <input type="number" name="jangka_waktu" value="{{ old('jangka_waktu', ($pendukung['jangka_waktu'] ?? '')) }}" class="form-control" placeholder="" aria-label="jangka_waktu" aria-describedby="jangkaWaktu">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="jangkaWaktu">Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Jenis Bunga</label>
                                            <select name="jenis_bunga" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="1" {{ old('jenis_bunga', ($pendukung['jenis_bunga'] ?? '')) == 1 ? 'selected' : '' }}>Menetap</option>
                                                <option value="2" {{ old('jenis_bunga', ($pendukung['jenis_bunga'] ?? '')) == 2 ? 'selected' : '' }}>Menurun</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Bunga</label>
                                            <select name="bunga" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="1.2" {{ old('bunga', ($pendukung['bunga'] ?? '')) == '1.2' ? 'selected' : '' }}>1.2%</option>
                                                <option value="1.5" {{ old('bunga', ($pendukung['bunga'] ?? '')) == '1.5' ? 'selected' : '' }}>1.5%</option>
                                                <option value="1.7" {{ old('bunga', ($pendukung['bunga'] ?? '')) == '1.7' ? 'selected' : '' }}>1.7%</option>
                                                <option value="2" {{ old('bunga', ($pendukung['bunga'] ?? '')) == '2' ? 'selected' : '' }}>2%</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Angsuran</label>
                                            <div class="input-group mb-3">
                                                <input type="number" name="angsuran" value="{{ old('angsuran', ($pendukung['angsuran'] ?? '')) }}" class="form-control" placeholder="">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="angsuran">Per Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Penghasilan</label>
                                            <div class="input-group mb-3">
                                                <input type="number" name="penghasilan" value="{{ old('penghasilan', ($pendukung['penghasilan'] ?? '')) }}" class="form-control" placeholder="">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="penghasilan">Per Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Jumlah Keluarga</label>
                                            <select name="jumlah_keluarga" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="1" {{ old('jumlah_keluarga', ($pendukung['jumlah_keluarga'] ?? '')) == "1" ? 'selected' : '' }}>1</option>
                                                <option value="2" {{ old('jumlah_keluarga', ($pendukung['jumlah_keluarga'] ?? '')) == "2" ? 'selected' : '' }}>2</option>
                                                <option value="3" {{ old('jumlah_keluarga', ($pendukung['jumlah_keluarga'] ?? '')) == "3" ? 'selected' : '' }}>3</option>
                                                <option value=">4" {{ old('jumlah_keluarga', ($pendukung['jumlah_keluarga'] ?? '')) == ">4" ? 'selected' : '' }}>>4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Jaminan</label>
                                            <select name="jaminan" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="BKPB Mobil" {{ old('jaminan', ($pendukung['jaminan'] ?? '')) == "BKPB Mobil" ? 'selected' : '' }}>BKPB Mobil</option>
                                                <option value="BKPB Motor" {{ old('jaminan', ($pendukung['jaminan'] ?? '')) == "BKPB Motor" ? 'selected' : '' }}>BKPB Motor</option>
                                                <option value="SHM" {{ old('jaminan', ($pendukung['jaminan'] ?? '')) == "SHM" ? 'selected' : '' }}>SHM</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Kepemilikan Rumah</label>
                                            <select name="kepemilikan_rumah" class="form-control">
                                                <option value="">- Pilih -</option>
                                                <option value="1" {{ old('kepemilikan_rumah', ($pendukung['kepemilikan_rumah'] ?? '')) == 1 ? 'selected' : '' }}>Miliki Sendiri</option>
                                                <option value="2" {{ old('kepemilikan_rumah', ($pendukung['kepemilikan_rumah'] ?? '')) == 2 ? 'selected' : '' }}>Ngontrak</option>
                                                <option value="3" {{ old('kepemilikan_rumah', ($pendukung['kepemilikan_rumah'] ?? '')) == 3 ? 'selected' : '' }}>Milik Orang Tua</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group text-right">
                        <input type="submit" value="Simpan" class="btn btn-success">
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
