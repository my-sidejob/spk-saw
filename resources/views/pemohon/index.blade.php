@extends('layouts.app')

@section('title', 'Pemohon')

@section('content')
    <div class="header bg-gray-dark pb-5">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Pemohon</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Filter Tanggal Pengajuan</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Dari</label>
                                    <input type="text" class="form-control datepicker" name="dari" value="{{ request()->get('dari') }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Sampai</label>
                                    <input type="text" class="form-control datepicker" name="sampai" value="{{ request()->get('sampai') }}">
                                </div>
                                <div class="col-md-12 text-right mt-3">
                                    <input type="submit" value="Cari" class="btn btn-info">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Data Pemohon / Alternatif</h3>
                            </div>
                            @if(auth()->user()->level == 1 || auth()->user()->level == 3)
                                <div class="col text-right">
                                    <a href="{{ route('pemohon.create') }}" class="btn btn-sm btn-primary">Tambah</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>TTL</th>
                                    <th>Tgl. Pengajuan</th>
                                    <th>Platform Pinjaman</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pemohon as $row)
                                    <tr>
                                        <td>A{{ $row->id }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>{{ $row->tmpt_lhr . ', ' . $row->tgl_lhr }}</td>
                                        <td>{{ $row->pendukung->tgl_pengajuan ?? 'N/A' }}</td>
                                        <td>{{ 'Rp.' . number_format($row->platform_pinjaman) }}</td>
                                        <td>
                                            <form action="{{ route('pemohon.destroy', $row->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('pendukung.show', $row->id) }}" class="btn btn-sm btn-success text-white">Detail & Pendukung</a>
                                                @if(auth()->user()->level == 1 || auth()->user()->level == 3)
                                                    <a href="{{ route('pemohon.edit', $row->id) }}" class="btn btn-sm btn-warning text-white" title="Edit"><i class="fa fa-cog"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm" title="Hapus" onclick="return confirm('Yakin untuk menghapus data?')"><i class="fa fa-trash"></i></button>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
