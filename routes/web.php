<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DetailKriteriaController;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\NormalisasiController;
use App\Http\Controllers\PemohonController;
use App\Http\Controllers\PendukungController;
use App\Http\Controllers\PenilaianController;
use App\Http\Controllers\PerhitunganController;
use App\Http\Controllers\RankingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::middleware(['web', 'auth'])->group(function(){
    Route::get('dashboard', [DashboardController::class, 'index']);
    Route::resource('kriteria', KriteriaController::class);
    Route::resource('detail-kriteria', DetailKriteriaController::class);
    Route::resource('pemohon', PemohonController::class);
    Route::resource('pendukung', PendukungController::class);
    Route::resource('penilaian', PenilaianController::class);
    Route::resource('user', UserController::class);
    Route::post('hitung', [PerhitunganController::class, 'hitung'])->name('hitung');
    Route::get('normalisasi', [NormalisasiController::class, 'index'])->name('normalisasi.index');
    Route::get('hasil', [RankingController::class, 'hasil'])->name('hasil.index');
    Route::get('print', [RankingController::class, 'print'])->name('ranking.print');

    Route::get('laporan-kredit', [RankingController::class, 'laporanKredit'])->name('laporan.kredit');
});

