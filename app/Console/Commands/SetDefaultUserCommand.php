<?php

namespace App\Console\Commands;

use App\Models\Pemohon;
use App\Models\Penilaian;
use App\Models\User;
use Illuminate\Console\Command;

class SetDefaultUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-default-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cs = User::where('level', 1)->first();
        Pemohon::whereNull('user_id')->update([
            'user_id' => $cs->id,
        ]);
        $ao = User::where('level', 2)->first();
        Penilaian::whereNull('user_id')->update([
            'user_id' => $ao->id,
        ]);
        echo "Success!";
    }
}
