<?php

namespace App\Services;

class NormalisasiService
{
    public function findMaxNumber(array $scores, string $jenis_kriteria)
    {
        if($jenis_kriteria === 'benefit') {
            return max($scores);
        } else {
            return min($scores);
        }
    }
}