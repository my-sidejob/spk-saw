<?php

namespace App\Services;

class PerhitunganService
{
    public function findPembagi(array $scores, string $jenis_kriteria)
    {
        if($jenis_kriteria === 'benefit') {
            return max($scores);
        } else {
            return min($scores);
        }
    }

    public function SAW($data)
    {
        $penilaianGroupped = $data->groupBy('kode')->all();
        $calculations = $data->groupBy('pemohon_id')->toArray();

        $pembagi = [];
        foreach($penilaianGroupped as $key => $kriteria) {
            $jenisKriteria = getDetailKriteria($key)->jenis_kriteria;
            $pembagi[$key] = $this->findPembagi($kriteria->pluck('skor')->all(), $jenisKriteria);
        }

        foreach($calculations as $key => $pemohon) {
            foreach($pemohon as $index => $calculate) {
                if($calculate['jenis_kriteria'] === 'benefit') {
                    $normalisasi = $calculate['skor'] / $pembagi[$calculate['kode']];
                    $calculations[$key][$index]['normalisasi'] = round($normalisasi, 1);
                } else {
                    $normalisasi = $pembagi[$calculate['kode']] / $calculate['skor'];
                    $calculations[$key][$index]['normalisasi'] = round($normalisasi, 1);
                }
                $result = $normalisasi * ($calculate['bobot'] / 100);
                $calculations[$key][$index]['result'] = round($result, 2);
            }
        }

        return $calculations;
    }

    public function calculateResult($data)
    {
        $rankingTable = [];

        foreach($data as $key => $row) {
            $total = 0;
            foreach($row as $calculation) {
                $total += $calculation['result'];
            }

            $rankingTable[$key] = [
                'pemohon_id' => $key,
                'total' => addZeroIfNeeded($total),
                'percentage' => $total * 100
            ];
        }
        usort($rankingTable, function ($a, $b) {
            return $b['percentage'] <=> $a['percentage'];
        });

        return $rankingTable;
    }
}