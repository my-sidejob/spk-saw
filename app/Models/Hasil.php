<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasil extends Model
{
    use HasFactory;

    protected $table = 'hasil';

    protected $fillable = [
        'pemohon_id',
        'total',
        'persentase',
    ];

    public function pemohon()
    {
        return $this->belongsTo('App\Models\Pemohon');
    }
}
