<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemohon extends Model
{
    use HasFactory;

    protected $table = 'pemohon';

    protected $fillable = [
        'nama',
        'tmpt_lhr',
        'tgl_lhr',
        'alamat',
        'j_kel',
        'status_nikah',
        'email',
        'no_telp',
        'platform_pinjaman',
        'user_id'
    ];

    public function pendukung()
    {
        return $this->hasOne('App\Models\Pendukung');
    }

    public static function getDefaultValues()
    {
        return [
            'nama' => '',
            'tmpt_lhr' => '',
            'tgl_lhr' => '',
            'alamat' => '',
            'j_kel' => '',
            'status_nikah' => '',
            'email' => '',
            'no_telp' => '',
            'platform_pinjaman' => '',
        ];
    }

    public function getStatusNikah()
    {
        if($this->status_nikah === 0) {
            return "Belum Menikah";
        } else {
            return "Menikah";
        }
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
