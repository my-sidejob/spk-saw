<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailKriteria extends Model
{
    use HasFactory;

    protected $table = 'detail_kriteria';

    protected $fillable = [
        'keterangan',
        'skor',
        'kriteria_id',
    ];

    public static function getDefaultValues()
    {
        return [
            'keterangan' => '',
            'skor' => '',
            'kriteria_id' => '',
        ];
    }
}
