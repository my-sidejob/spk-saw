<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    use HasFactory;

    protected $table = 'penilaian';

    protected $fillable = [
        'pemohon_id',
        'detail_kriteria_id',
        'user_id'
    ];

    public static function getDefaultValues()
    {
        return [
            'pemohon_id' => '',
            'detail_kriteria_id' => ''
        ];
    }

    public function detailKriteria()
    {
        return $this->belongsTo('App\Models\DetailKriteria');
    }

    public function scopeOrderByKodeKriteria($query)
    {
        return $query->join('detail_kriteria', 'detail_kriteria.id', 'penilaian.detail_kriteria_id')
                        ->join('kriteria', 'kriteria.id', 'detail_kriteria.kriteria_id')
                        ->orderBy('kode', 'asc');
    }

    public function findMaxNumber(array $detail_kriteria_ids)
    {

    }
}
