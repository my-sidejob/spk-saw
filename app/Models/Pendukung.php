<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendukung extends Model
{
    use HasFactory;

    use HasFactory;

    protected $table = 'pendukung';

    protected $fillable = [
        'pemohon_id',
        'status_bi_checking', //1 lancar, 2 diragukan, 3 DPK, 4 tidak lancar
        'jangka_waktu',
        'jenis_bunga', //1, menetap, 2 menurun
        'bunga',
        'angsuran',
        'penghasilan',
        'tgl_pengajuan',
        'jumlah_keluarga',
        'jaminan',
        'kepemilikan_rumah', //1 Milik sendiri, 2 Mengontrak, 3 Milik Orang Tua
    ];

    public function getStatusBIChecking()
    {
        if($this->status_bi_checking === 1) {
            return "Lancar";
        } else if($this->status_bi_checking === 2) {
            return "Diragukan";
        } else if($this->status_bi_checking === 3) {
            return "DPK";
        } else {
            return "Tidak Lancar";
        }
    }

    public function getJenisBunga()
    {
        if($this->jenis_bunga === 1) {
            return "Menetap";
        } else {
            return "Menurun";
        }
    }

    public function getKepemilikanRumah()
    {
        if($this->kepemilikan_rumah === 1) {
            return "Milik Sendiri";
        } else if($this->kepemilikan_rumah === 2) {
            return "Mengontrak";
        } else {
            return "Milik Orang Tua";
        }
    }

    public static function getDefaultValues()
    {
        return [
            'pemohon_id' => '',
            'status_bi_checking' => '', //1 lancar, 2 diragukan, 3 DPK, 4 tidak lancar
            'jangka_waktu' => '',
            'jenis_bunga' => '', //1, menetap, 2 menurun
            'bunga' => '',
            'angsuran' => '',
            'penghasilan' => '',
            'tgl_pengajuan' => date('Y-m-d'),
            'jumlah_keluarga' => '',
            'jaminan' => '',
            'kepemilikan_rumah' => '',
        ];
    }


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
