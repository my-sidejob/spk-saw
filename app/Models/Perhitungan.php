<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perhitungan extends Model
{
    use HasFactory;

    protected $table = 'perhitungan';

    protected $fillable = [
        'pemohon_id',
        'kriteria_id',
        'matrix',
        'normalisasi',
        'hasil',
    ];

    public function pemohon()
    {
        return $this->belongsTo('App\Models\Pemohon');
    }

    public function kriteria()
    {
        return $this->belongsTo('App\Models\Kriteria');
    }
}
