<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Kriteria extends Model
{
    use HasFactory;

    protected $table = 'kriteria';

    protected $fillable = [
        'kode',
        'nama',
        'jenis_kriteria',
        'bobot',
        'keterangan',
    ];

    public function setKodeAttribute($value)
    {
        $this->attributes['kode'] = Str::upper($value);
    }

    public static function getDefaultValues()
    {
        return [
            'kode' => '',
            'nama' => '',
            'jenis_kriteria' => '',
            'bobot' => '',
            'keterangan' => ''
        ];
    }

    public function getJenisKriteria()
    {
        if($this->jenis_kriteria == 'benefit') {
            return '<span class="badge badge-primary">Benefit</span>';
        } else {
            return '<span class="badge badge-warning">Cost</span>';
        }
    }

    public function detail_kriteria()
    {
        return $this->hasMany('App\Models\DetailKriteria');
    }
}
