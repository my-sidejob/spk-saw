<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'nama',
        'email',
        'password',
        'alamat',
        'no_telp',
        'level', // 1. CS, 2. Account Officer, 3. Admin
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }

    public static function getDefaultValues()
    {
        return [
            'nama' => '',
            'email' => '',
            'password' => '',
            'alamat' => '',
            'no_telp' => '',
            'level' => '',
        ];
    }

    public function getLevel()
    {
        if ($this->level === 1) {
            return 'Customer Service';
        }

        if ($this->level === 2) {
            return 'Account Officer';
        }

        if ($this->level === 3) {
            return 'Admin';
        }

        if ($this->level == 4) {
            return 'Direksi';
        }
    }
}
