<?php

use App\Models\Kriteria;
use App\Models\Pemohon;
use App\Models\User;

function urlHasPrefix(string $prefix) {
    $url = url()->current();
    if (strpos($url, $prefix) > 0) {
        return true;
    }

    return false;
}

function getDetailAlternatif($id)
{
    return Pemohon::find($id);
}

function getDetailKriteria($kode)
{
    return Kriteria::where('kode', $kode)->first();
}

function checkKelayakan($number)
{
    return $number >= 50 ? '<span class="badge badge-primary">Layak</span>' : '<span class="badge badge-danger">Tidak</span>';
}

function getStatusKredit($status)
{
    if ($status == 1) {
        return 'Layak';
    } else {
        return 'Tidak Layak';
    }
}

function monthName() {
    return [
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'Mei',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Agu',
        9 => 'Sep',
        10 => 'Okt',
        11 => 'Nov',
        12 => 'Des'
    ];
}

function getUser($id)
{
    return User::find($id);
}

function addZeroIfNeeded($number)
{
    $length = strlen((string)$number);
    if($length === 3) {
        $number = $number.'0';
    }

    return $number;
}