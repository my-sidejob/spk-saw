<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = User::all();
        return view('user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->level !== 3) {
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['user'] = User::getDefaultValues();

        return view('user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email',
            'nama' => 'required',
            'level'=> 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'password' => 'required',
        ]);

        User::create($request->toArray());
        return redirect()->route('user.index')->with('success', 'Berhasil menambah data user');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->level !== 3) {
            if(auth()->user()->id != $id) {
                return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
            }
        }
        $data['user'] = User::findOrFail($id);
        return view('user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required',
            'nama' => 'required',
            'level'=> 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
        ]);
        $input = $request->toArray();
        if(empty($input['password'])){
            unset($input['password']);
        }
        User::findOrfail($id)->update($input);
        return redirect()->route('user.index')->with('success', 'Berhasil mengubah data user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrfail($id)->delete();
        return redirect()->route('user.index')->with('success', 'Berhasil menghapus data user');
    }
}
