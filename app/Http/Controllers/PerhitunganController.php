<?php

namespace App\Http\Controllers;

use App\Models\Hasil;
use App\Models\Penilaian;
use App\Models\Perhitungan;
use App\Services\PerhitunganService;

class PerhitunganController extends Controller
{
    protected $perhitunganService;

    public function __construct()
    {
        $this->perhitunganService = new PerhitunganService();
    }

    public function hitung()
    {
        $penilaian = Penilaian::select('pemohon_id', 'detail_kriteria_id', 'skor', 'kode', 'kriteria.id as kriteria_id', 'jenis_kriteria', 'bobot')
                                ->orderByKodeKriteria()
                                ->orderBy('pemohon_id')
                                ->get();

        $results = $this->perhitunganService->SAW($penilaian);

        Perhitungan::truncate();

        foreach ($results as $pemohons) {
            foreach ($pemohons as $pemohon) {
                Perhitungan::updateOrCreate(
                    [
                        'pemohon_id' => $pemohon['pemohon_id'],
                        'kriteria_id' => $pemohon['kriteria_id']
                    ],
                    [
                        'matrix' => $pemohon['skor'],
                        'normalisasi' => $pemohon['normalisasi'],
                        'hasil' => $pemohon['result'],
                    ]
                );
            }
        }

        Hasil::truncate();
        $finalResults = $this->perhitunganService->calculateResult($results);
        foreach ($finalResults as $result) {
            Hasil::updateOrCreate(
                [
                    'pemohon_id' => $result['pemohon_id']
                ],
                [
                    'total' => $result['total'],
                    'persentase' => $result['percentage']
                ]
            );
        }

        return redirect()->back()->with('success', 'Perhitungan berhasil');
        //dd($hasil);
    }
}
