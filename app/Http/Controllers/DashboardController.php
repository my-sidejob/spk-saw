<?php

namespace App\Http\Controllers;

use App\Models\DetailKriteria;
use App\Models\Hasil;
use App\Models\Kriteria;
use App\Models\Pemohon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $data['total_pemohon'] = Pemohon::all()->count();
        $data['total_kriteria'] = Kriteria::all()->count();
        $data['total_aspek_penilaian'] = DetailKriteria::all()->count();
        $data['tahun'] = Pemohon::selectRaw("year(created_at) as year")->get()->groupBy('year');
        $year = request()->get('year') ?? date('Y');
        $totalKredit = Pemohon::selectRaw("count(pemohon.id) as total, month(tgl_pengajuan) as month")
            ->join('pendukung', 'pendukung.pemohon_id', '=', 'pemohon.id')
            ->whereYear('tgl_pengajuan', $year)
            ->groupBy('month')
            ->orderBy('month')
            ->get();

        $chartTotalKredit = $this->convertToDataChart($totalKredit);

        $data['monthTotalKredit'] = $chartTotalKredit['months'];

        $data['dataTotalKredit'] = $chartTotalKredit['total'];

        $layak = Hasil::selectRaw("count(hasil.pemohon_id) as total, month(tgl_pengajuan) as month")
            ->join('pendukung', 'pendukung.pemohon_id', '=', 'hasil.pemohon_id')
            ->where('persentase', '>=', 50)
            ->whereYear('tgl_pengajuan', $year)
            ->groupBy('month')
            ->orderBy('month')
            ->get();
        $chartLayak = $this->convertToDataChart($layak);
        $data['monthLayak'] = $chartLayak['months'];
        $data['dataLayak'] = $chartLayak['total'];

        $tidakLayak = Hasil::selectRaw("count(hasil.pemohon_id) as total, month(tgl_pengajuan) as month")
            ->join('pendukung', 'pendukung.pemohon_id', '=', 'hasil.pemohon_id')
            ->where('persentase', '<', 50)
            ->whereYear('tgl_pengajuan', $year)
            ->groupBy('month')
            ->orderBy('month')
            ->get();
        $chartTidakLayak = $this->convertToDataChart($tidakLayak);
        $data['monthTidakLayak'] = $chartTidakLayak['months'];
        $data['dataTidakLayak'] = $chartTidakLayak['total'];

        return view('dashboard', $data);
    }

    private function convertToDataChart($data)
    {
        $month = '';
        $total = '';
        foreach ($data as $group) {
            $month .= monthName()[$group['month']].',';
            $total .= $group['total'].',';
        }

        return [
            'months' => rtrim($month, ','),
            'total' => rtrim($total, ','),
        ];
    }
}
