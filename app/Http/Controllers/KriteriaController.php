<?php

namespace App\Http\Controllers;

use App\Models\DetailKriteria;
use App\Models\Kriteria;
use Illuminate\Http\Request;

class KriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }

        $data['kriteria'] = Kriteria::all();
        return view('kriteria.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['kriteria'] = Kriteria::getDefaultValues();

        return view('kriteria.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required|unique:kriteria,nama',
            'nama' => 'required|unique:kriteria,nama',
            'jenis_kriteria'=> 'required',
            'bobot' => 'required',
        ]);

        Kriteria::create($request->toArray());
        return redirect()->route('kriteria.index')->with('success', 'Berhasil menambah data kriteria');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['kriteria'] = Kriteria::find($id);
        $data['table_detail_kriteria'] = DetailKriteria::where('kriteria_id', $id)->orderBy('skor', 'asc')->get();
        $data['detail_kriteria'] = DetailKriteria::getDefaultValues();
        return view('kriteria.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kriteria'] = Kriteria::findOrFail($id);
        return view('kriteria.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'jenis_kriteria'=> 'required',
            'bobot' => 'required',
        ]);

        Kriteria::findOrfail($id)->update($request->toArray());
        return redirect()->route('kriteria.index')->with('success', 'Berhasil mengubah data kriteria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kriteria::findOrfail($id)->delete();
        return redirect()->route('kriteria.index')->with('success', 'Berhasil menghapus data kriteria');
    }
}
