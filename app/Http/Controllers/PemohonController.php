<?php

namespace App\Http\Controllers;

use App\Models\Pemohon;
use App\Models\Pendukung;
use Illuminate\Http\Request;

class PemohonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->get('dari') == null){
            $data['pemohon'] = Pemohon::all();
        } else {
            $data['pemohon'] = Pemohon::join('pendukung', 'pendukung.pemohon_id', 'pemohon.id')
                                ->whereBetween('tgl_pengajuan', [request()->get('dari'), request()->get('sampai')])
                                ->get();
        }
        return view('pemohon.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->level == 2){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['pemohon'] = Pemohon::getDefaultValues();
        $data['pendukung'] = Pendukung::getDefaultValues();
        return view('pemohon.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' => 'required',
            'alamat' => 'required',
            'j_kel' => 'required',
            'status_nikah' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'platform_pinjaman' => 'required',
            'status_bi_checking' => 'required',
            'jangka_waktu' => 'required',
            'jenis_bunga' => 'required',
            'bunga' => 'required',
            'angsuran' => 'required',
            'penghasilan' => 'required',
            'tgl_pengajuan' => 'required',
            'jumlah_keluarga' => 'required',
            'jaminan' => 'required',
            'kepemilikan_rumah' => 'required',
        ]);
        $input = $request->toArray();
        $input['user_id'] = auth()->user()->id;
        $pemohon = Pemohon::create($input);
        Pendukung::create([
            'pemohon_id' => $pemohon->id,
            'status_bi_checking' => $input['status_bi_checking'],
            'jangka_waktu' => $input['jangka_waktu'],
            'jenis_bunga' => $input['jenis_bunga'],
            'bunga' => $input['bunga'],
            'angsuran' => $input['angsuran'],
            'penghasilan' => $input['penghasilan'],
            'tgl_pengajuan' => $input['tgl_pengajuan'],
            'jumlah_keluarga' => $input['jumlah_keluarga'],
            'jaminan' => $input['jaminan'],
            'kepemilikan_rumah' => $input['kepemilikan_rumah'],
        ]);
        return redirect()->route('pemohon.index')->with('success', 'Berhasil menambah data pemohon');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pemohon  $pemohon
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pemohon  $pemohon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->level == 2){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['pemohon'] = Pemohon::findOrFail($id);
        $data['pendukung'] = Pendukung::where('pemohon_id', $id)->first();
        return view('pemohon.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pemohon  $pemohon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'tmpt_lhr' => 'required',
            'tgl_lhr' => 'required',
            'alamat' => 'required',
            'j_kel' => 'required',
            'status_nikah' => 'required',
            'email' => 'required',
            'no_telp' => 'required',
            'platform_pinjaman' => 'required',
            'status_bi_checking' => 'required',
            'jangka_waktu' => 'required',
            'jenis_bunga' => 'required',
            'bunga' => 'required',
            'angsuran' => 'required',
            'penghasilan' => 'required',
            'tgl_pengajuan' => 'required',
            'jumlah_keluarga' => 'required',
            'jaminan' => 'required',
            'kepemilikan_rumah' => 'required',
        ]);
        $input = $request->toArray();
        $input['user_id'] = auth()->user()->id;
        Pemohon::findOrfail($id)->update($input);
        Pendukung::updateOrCreate(
            ['pemohon_id' => $id],
            [
                'status_bi_checking' => $input['status_bi_checking'],
                'jangka_waktu' => $input['jangka_waktu'],
                'jenis_bunga' => $input['jenis_bunga'],
                'bunga' => $input['bunga'],
                'angsuran' => $input['angsuran'],
                'penghasilan' => $input['penghasilan'],
                'tgl_pengajuan' => $input['tgl_pengajuan'],
                'jumlah_keluarga' => $input['jumlah_keluarga'],
                'jaminan' => $input['jaminan'],
                'kepemilikan_rumah' => $input['kepemilikan_rumah'],
            ]
        );
        return redirect()->route('pemohon.index')->with('success', 'Berhasil mengubah data pemohon');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pemohon  $pemohon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pemohon::findOrfail($id)->delete();
        return redirect()->route('pemohon.index')->with('success', 'Berhasil menghapus data pemohon');
    }
}
