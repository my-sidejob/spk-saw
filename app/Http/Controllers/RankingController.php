<?php

namespace App\Http\Controllers;

use App\Models\Hasil;
use App\Models\Kriteria;
use App\Models\Penilaian;
use App\Services\PerhitunganService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RankingController extends Controller
{
    protected $perhitunganService;

    public function __construct()
    {
        $this->perhitunganService = new PerhitunganService();
    }

    public function hasil()
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['kriteria'] = Kriteria::orderBy('kode')->get();

        $data['ranking'] = Hasil::with('pemohon')->orderBy('persentase', 'desc')->get();

        return view('ranking.index', $data);
    }

    public function laporanKredit()
    {
        $data['kriteria'] = Kriteria::orderBy('kode')->get();

        $data['ranking'] = Hasil::with('pemohon')->orderBy('persentase', 'desc')->get();

        return view('laporan.kredit', $data);
    }

    public function print()
    {
        $data['kriteria'] = Kriteria::orderBy('kode')->get();

        $data['ranking'] = Hasil::with('pemohon')->orderBy('persentase', 'desc')->get();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('ranking.print', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();

    }
}
