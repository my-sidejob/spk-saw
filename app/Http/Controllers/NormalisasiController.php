<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use App\Models\Penilaian;
use App\Models\Perhitungan;
use App\Services\PerhitunganService;

class NormalisasiController extends Controller
{
    public function index()
    {

        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $data['kriteria'] = Kriteria::all();
        $dataPerhitungan = Perhitungan::with('pemohon')->with('kriteria')->orderBy('pemohon_id', 'asc')->orderBy('kriteria_id', 'asc')->get();
        $data['perhitungan'] = $dataPerhitungan->groupBy('pemohon_id');
        return view('normalisasi.index', $data);
    }
}
