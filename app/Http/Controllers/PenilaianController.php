<?php

namespace App\Http\Controllers;

use App\Models\Kriteria;
use App\Models\Pemohon;
use App\Models\Penilaian;
use App\Models\Perhitungan;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $penilaian = Penilaian::select('pemohon_id', 'detail_kriteria_id', 'kode')
                                ->orderByKodeKriteria()
                                ->get();
        $penilaian = $penilaian->groupBy('pemohon_id');
        $penilaian = $penilaian->sort();
        $penilaian = $penilaian->all();
        $data['penilaian'] = $penilaian;
        $data['kriteria'] = Kriteria::orderBy('kode')->get();
        $dataPerhitungan = Perhitungan::with('pemohon')->with('kriteria')->orderBy('pemohon_id', 'asc')->orderBy('kriteria_id', 'asc')->get();
        $data['perhitungan'] = $dataPerhitungan->groupBy('pemohon_id');
        return view('penilaian.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $filled = Penilaian::select('pemohon_id')->distinct()->get()->pluck('pemohon_id')->all();
        $data['pemohon'] = Pemohon::whereNotIn('id', $filled)->get();
        $data['kriteria'] = Kriteria::orderBy('kode', 'asc')->get();
        $data['penilaian'] = Penilaian::getDefaultValues();
        $data['type'] = 'create';
        return view('penilaian.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'pemohon_id' => 'required',
        ]);
        foreach($request['detail_kriteria_ids'] as $detail_kriteria_id) {
            Penilaian::create([
                'pemohon_id' => $request['pemohon_id'],
                'detail_kriteria_id' => $detail_kriteria_id,
                'user_id' => auth()->user()->id,
            ]);
        }

        return redirect()->route('penilaian.index')->with('success', 'Berhasil menyimpan penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pemohon_id)
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $penilaian = Penilaian::select('pemohon_id', 'detail_kriteria_id', 'kode', 'detail_kriteria.keterangan as ket', 'user_id')
                    ->orderByKodeKriteria()
                    ->where('pemohon_id', $pemohon_id)
                    ->get();
        $data['penilaian'] = $penilaian->groupBy('kode')->all();
        $data['pemohon'] = Pemohon::findOrFail($pemohon_id);

        return view('penilaian.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($pemohon_id)
    {
        if(auth()->user()->level == 1){
            return redirect('dashboard')->with('warning', 'Anda tidak memiliki akses');
        }
        $penilaian = Penilaian::select('pemohon_id', 'detail_kriteria_id', 'kode')
                    ->orderByKodeKriteria()
                    ->where('pemohon_id', $pemohon_id)
                    ->get();
        $data['penilaian'] = $penilaian->groupBy('kode')->all();
        $data['pemohon'] = Pemohon::findOrFail($pemohon_id);
        $data['kriteria'] = Kriteria::orderBy('kode', 'asc')->get();
        $data['type'] = 'edit';

        return view('penilaian.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pemohon_id)
    {
        $request->validate([
            'pemohon_id' => 'required',
        ]);

        Penilaian::where('pemohon_id', $pemohon_id)->delete();

        foreach($request['detail_kriteria_ids'] as $detail_kriteria_id) {
            Penilaian::create([
                'pemohon_id' => $request['pemohon_id'],
                'detail_kriteria_id' => $detail_kriteria_id,
                'user_id' => auth()->user()->id,
            ]);
        }

        return redirect()->route('penilaian.index')->with('success', 'Berhasil mengubah penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($pemohon_id)
    {
        Penilaian::where('pemohon_id', $pemohon_id)->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus penilaian');
    }
}
