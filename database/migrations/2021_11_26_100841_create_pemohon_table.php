<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemohonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemohon', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('tmpt_lhr');
            $table->date('tgl_lhr');
            $table->string('alamat');
            $table->string('j_kel', 2);
            $table->integer('status_nikah'); //0 belum menikah, 1 menikah
            $table->string('email');
            $table->string('no_telp');
            $table->string('platform_pinjaman');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemohon');
    }
}
