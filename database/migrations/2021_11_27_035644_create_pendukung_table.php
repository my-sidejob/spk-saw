<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendukungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendukung', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pemohon_id');
            $table->integer('status_bi_checking'); //1 lancar, 2 diragukan, 3 DPK, 4 tidak lancar
            $table->integer('jangka_waktu');
            $table->integer('jenis_bunga'); //1, menetap, 2 menurun
            $table->string('bunga');
            $table->float('angsuran');
            $table->integer('penghasilan');
            $table->date('tgl_pengajuan');
            $table->string('jumlah_keluarga'); //1, 2, 3, 4<
            $table->string('jaminan');
            $table->integer('kepemilikan_rumah'); //1 milik sendiri, 2 ngontrak, 3 milik orang tua
            $table->foreign('pemohon_id')->references('id')->on('pemohon')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendukung');
    }
}
